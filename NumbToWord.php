<?php

class NumbToWord
{
    private $text;
    private $exponent;
    private $gend;
    private $result;
    private static $inst = null;

    private function isDigit($ind, $start){return !($ind < $start || ($ind - $start) % 3 != 0);}

    private function getExponent($key, $ind)
    {
        $str = '';
        $key = round($key > (count($this->exponent) - 1) * 3 + 1 ? 0 : $key / 3);
        if ($key == 1) {
            switch ($ind) {
                case 0:
                    $str = 'а';
                    break;
                case 1:
                    $str = 'и';
                    break;
            }
        } else {
            switch ($ind) {
                case 1:
                    $str = 'а';
                    break;
                case 2:
                    $str = 'ов';
                    break;
            }
        }
        return $this->exponent[$key] . $str;
    }

    private function getText($key, $ind, $gend)
    {
        $ind = $key == 'unitsT' ? $ind - 1 : $ind;
        if($key == 'units' && $this->gend == 'w' && in_array($ind, array(1,2)) && $gend)
            return $this->text['unitsT'][$ind-1];
        else
            return $this->text[$key][$ind];
    }
    private function __construct($gend = 'm')
    {
        $this->gend = $gend;
        $this->result = array();
        $this->text = array(
            'units'     =>  array('ноль', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'),
            'unitsT'    => array('одна', 'две'),
            'to20'      => array('одинадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семьнадцать', 'восемнадцать', 'девятнадцать'),
            'decade'    => array('десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'),
            'hundreds'  => array('сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот')
        );
        $this->exponent = array(
            '...ллион',             // для значений больших чем данный список
            'тысяч',                //10^3
            'миллион',              //10^6
            'миллиард',             //10^9
            'триллион',             //10^12
            'квадриллион',          //10^15
            'квинтиллион',          //10^18
            'секстиллион',          //10^21
            'септиллион',           //10^24
            'октиллион',            //10^27
            'нониллион',            //10^30
            'дециллион',            //10^33
            'андециллион',          //10^36
            'дуодециллион',         //10^39
            'тредециллион',         //10^42
            'кваттуордециллион',    //10^45
            'квиндециллион',        //10^48
            'седециллион',          //10^51
            'септдециллион',        //10^54
            'октодециллион',        //10^57
            'новемдециллион',       //10^60
            'вигинтиллион',         //10^63
            'анвигинтиллион',       //10^66
            'дуовигинтиллион',      //10^69
            'тревигинтиллион',      //10^72
            'кватторвигинтиллион',  //10^75
            'квинвигинтиллион',     //10^78
            'сексвигинтиллион',     //10^81
            'септемвигинтиллион',   //10^84
            'октовигинтиллион',     //10^87
            'новемвигинтиллион',    //10^90
            'тригинтиллион',        //10^93
            'антригинтиллион',      //10^96
            'дуотригинтиллион',     //10^99
        );
    }
    public static function instance()
    {
        if(self::$inst === null)
            self::$inst = new self();
        return self::$inst;
    }
    public function setGender($gend = 'm')
    {
        $this->gend = $gend;
        return self::$inst;
    }

    public function convert($numb)
    {
        $this->result = array();
        $arr = array_reverse(str_split($numb));
        $fl = false;
        $len = count($arr);
        $raz = 3;
        while (!$this->isDigit($len, $raz--)) ;
        $raz++;
        do {
            if ($arr[$len - 1] != 0 && $len != 0) {
                $fl = true;
                $gend = false;
                $index = $arr[$len - 1] - 1;
                switch ($raz) {
                    case 1: // единицы
                        $key = ($arr[$len - 1] < 3 && $len == 4) ? 'unitsT': 'units';
                        $gend = $len == 1;
                        $index++;
                        break;
                    case 2: // десятки
                        if ($arr[$len - 1] < 2 && $arr[$len - 2] != 0) {
                            $key = 'to20';
                            $index = $arr[$len - 2] - 1;
                            $arr[$len - 2] = 0;
                        } else
                            $key = 'decade';
                        break;
                    case 3: // сотни
                        $key = 'hundreds';
                        break;
                }
                $this->result[] = $this->getText($key, $index, $gend);
            }

            if ($fl && $this->isDigit($len, 4)) {
                $fl = false;
                $nch = (count($arr) - $len) >= 1 ? $arr[$len] * 10 + $arr[$len - 1] : $arr[$len - 1];
                // какое окончание добавлять
                if ($nch == 0 || $nch >= 5 && $nch < 20 || $nch % 10 == 0 || $nch % 10 >= 5 && $nch % 10 < 10)
                    $ind = 2;
                elseif ($nch == 1 || $nch % 10 == 1)
                    $ind = 0;
                else
                    $ind = 1;
                $this->result[] = $this->getExponent($len - 1, $ind);
            }
            $raz == 1 ? $raz = 3 : --$raz;
        } while (--$len);
        unset($arr);
        unset($arr);
        return self::$inst;
    }
    public function getString(){return implode(' ',$this->result);}
    public function getArray(){return $this->result;}
}